-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 28 Kwi 2016, 02:47
-- Wersja serwera: 5.6.21
-- Wersja PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `data`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `grupa_materialow`
--

CREATE TABLE IF NOT EXISTS `grupa_materialow` (
`id` int(11) NOT NULL,
  `nazwa` varchar(50) COLLATE utf16_polish_ci NOT NULL,
  `rodzic` varchar(50) COLLATE utf16_polish_ci NOT NULL,
  `dziecko` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `jednostka_miary`
--

CREATE TABLE IF NOT EXISTS `jednostka_miary` (
`id` int(11) NOT NULL,
  `nazwa` varchar(50) COLLATE utf16_polish_ci NOT NULL,
  `skrot` varchar(50) COLLATE utf16_polish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `material`
--

CREATE TABLE IF NOT EXISTS `material` (
`id` int(11) NOT NULL,
  `kod` varchar(50) COLLATE utf16_polish_ci NOT NULL,
  `nazwa` varchar(50) COLLATE utf16_polish_ci NOT NULL,
  `id_jednostka_miary` int(11) DEFAULT NULL,
  `id_grupy_materialow` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf16 COLLATE=utf16_polish_ci;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `grupa_materialow`
--
ALTER TABLE `grupa_materialow`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jednostka_miary`
--
ALTER TABLE `jednostka_miary`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `material`
--
ALTER TABLE `material`
 ADD PRIMARY KEY (`id`), ADD KEY `id_jednostka_miary` (`id_jednostka_miary`), ADD KEY `id_grupy_materialow` (`id_grupy_materialow`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `grupa_materialow`
--
ALTER TABLE `grupa_materialow`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT dla tabeli `jednostka_miary`
--
ALTER TABLE `jednostka_miary`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `material`
--
ALTER TABLE `material`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `material`
--
ALTER TABLE `material`
ADD CONSTRAINT `material_ibfk_1` FOREIGN KEY (`id_jednostka_miary`) REFERENCES `jednostka_miary` (`id`) ON UPDATE NO ACTION,
ADD CONSTRAINT `material_ibfk_2` FOREIGN KEY (`id_grupy_materialow`) REFERENCES `grupa_materialow` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
