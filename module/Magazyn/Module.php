<?php

namespace Magazyn;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Magazyn\Model\Material;
use Magazyn\Model\MaterialTable;
use Magazyn\Model\Jednostki;
use Magazyn\Model\JednostkiTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;


class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig()
     {
         return array(
             'factories' => array(
                 'Magazyn\Model\MaterialTable' =>  function($sm) {
                     $tableGateway = $sm->get('MaterialTableGateway');
                     $table = new Model\MaterialTable($tableGateway);
                     return $table;
                 },
                 'MaterialTableGateway' => function ($sm) {
                     $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                     $resultSetPrototype = new ResultSet();
                     $resultSetPrototype->setArrayObjectPrototype(new Model\Material());
                     return new TableGateway('material', $dbAdapter, null, $resultSetPrototype);
                 },
                 'Magazyn\Model\JednostkiTable' =>  function($sm) {
                     $tableGateway = $sm->get('JednostkiTableGateway');
                     $table = new Model\JednostkiTable($tableGateway);
                     return $table;
                 },
                 'JednostkiTableGateway' => function ($sm) {
                     $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                     $resultSetPrototype = new ResultSet();
                     $resultSetPrototype->setArrayObjectPrototype(new Model\Jednostki());
                     return new TableGateway('jednostka_miary', $dbAdapter, null, $resultSetPrototype);
                 },
                 'Magazyn\Model\GrupyTable' =>  function($sm) {
                     $tableGateway = $sm->get('GrupyTableGateway');
                     $table = new Model\GrupyTable($tableGateway);
                     return $table;
                 },
                 'GrupyTableGateway' => function ($sm) {
                     $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                     $resultSetPrototype = new ResultSet();
                     $resultSetPrototype->setArrayObjectPrototype(new Model\Grupy());
                     return new TableGateway('grupa_materialow', $dbAdapter, null, $resultSetPrototype);
                 },
             ),
         );
     }

}
