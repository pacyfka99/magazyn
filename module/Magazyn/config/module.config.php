<?php


namespace Magazyn;

return array(
     'controllers' => array(
         'invokables' => array(
             'Magazyn\Controller\Material' => 'Magazyn\Controller\MaterialController',
             'Magazyn\Controller\Jednostki' => 'Magazyn\Controller\JednostkiController',
             'Magazyn\Controller\Grupy' => 'Magazyn\Controller\GrupyController',
         ),
     ),
     'router' => array(
         'routes' => array(
             'material' => array(
                 'type'    => 'segment',
                 'options' => array(
                     'route'    => '/material[/:action][/:id]',
                     'constraints' => array(
                         'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                         'id'     => '[0-9]+',
                     ),
                     'defaults' => array(
                         'controller' => 'Magazyn\Controller\Material',
                         'action'     => 'index',
                     ),
                 ),
             ),
             'jednostki' => array(
                 'type'    => 'segment',
                 'options' => array(
                     'route'    => '/jednostki[/:action][/:id]',
                     'constraints' => array(
                         'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                         'id'     => '[0-9]+',
                     ),
                     'defaults' => array(
                         'controller' => 'Magazyn\Controller\Jednostki',
                         'action'     => 'index',
                     ),
                 ),
             ),
             'grupy' => array(
                 'type'    => 'segment',
                 'options' => array(
                     'route'    => '/grupy[/:action][/:id]',
                     'constraints' => array(
                         'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                         'id'     => '[0-9]+',
                     ),
                     'defaults' => array(
                         'controller' => 'Magazyn\Controller\Grupy',
                         'action'     => 'index',
                     ),
                 ),
             ),
         ),
     ),

     'view_manager' => array(
         'template_path_stack' => array(
             'material' => __DIR__ . '/../view',
             'jednostki' => __DIR__ . '/../view',
             'grupy' => __DIR__ . '/../view',
         ),
     ),
 );
