<?php

 namespace Magazyn\Model;
 
 use Zend\Db\TableGateway\TableGateway;
 use Zend\Db\Sql\Select;
 use Zend\Db\Sql\Where;

 class GrupyTable
 {
     protected $tableGateway;

     public function __construct(TableGateway $tableGateway)
     {
  
         $this->tableGateway = $tableGateway;
     }

     public function fetchAll()
     {
          $resultSet = $this->tableGateway->select(function(Select $select){
            $select->order('rodzic ASC');
                
    });
         return $resultSet;
    }
     
     public function getChildren()
     {
          $where = new Where();    
          $where->EqualTo('dziecko' , 1);
          $result = $this->tableGateway->select($where);
                
         return $result;
     }
     
      public function fetchAllGroup()
     {
         $resultSet = $this->tableGateway->select(function(Select $select){
            $select->order('rodzic ASC');
            $select->join('material', 'grupa_materialow.id <> material.id_grupy_materialow',array());     
    });

         return $resultSet;
     }
     
     public function getById($id)
     {
         $id  = (int) $id;
         $rowset = $this->tableGateway->select(array('id' => $id));
         $row = $rowset->current();
         if (!$row) {
             throw new \Exception("Could not find row $id");
         }
         return $row;
     }

     public function save(Grupy $grupy)
     {
         $lastParent = null;
         $newParent = null;
         
         $data = array(
             'nazwa'  => $grupy->nazwa,
         );
         
         $id = (int) $grupy->id;
         if ($id == 0) {      
             if ($this->tableGateway->insert($data)) {
                 $lastId = $this->tableGateway->lastInsertValue;
          
                 $data['rodzic'] = $lastId;
                 if($grupy->rodzic != 'brak'){
                    $newParent = $grupy->rodzic;
                    $data['rodzic'] = $grupy->rodzic.'.'.$lastId;
                 }

                 $this->update($data, $lastId);
             } else {
                 throw new \Exception('Grupa nie istnieje');
             }
         } else {
             $currentGroup = $this->getById($id);
             $this->update($data, $currentGroup->id);
             unset($data['nazwa']);
             if ($currentGroup) {
                 if(substr_count($currentGroup->rodzic, '.') == 0){
                     if($grupy->rodzic != 'brak'){
                        $newParent = $grupy->rodzic;
                        $data['rodzic'] = $grupy->rodzic.'.'.$currentGroup->rodzic;
                        $this->update($data, $currentGroup->id);
                       
                        $where = new Where();    
                        $where->like('rodzic', $currentGroup->rodzic.'.%');
                        $where->notEqualTo('id' , $currentGroup->id);
                        $result = $this->tableGateway->select($where);
                        foreach ($result as $current){
                            $data['rodzic'] = $grupy->rodzic.'.'.$current->rodzic;
                            $this->update($data, $current->id);
                        }
                    }
                 }else{
                     if($grupy->rodzic != 'brak'){
                        $newParent = $grupy->rodzic;
                        $lastParent = substr($currentGroup->rodzic, 0,-strlen($currentGroup->id)-1); 
                        $data['rodzic'] = $grupy->rodzic.'.'.$currentGroup->id;
                        $this->update($data, $currentGroup->id);

                        $where = new Where();    
                        $where->like('rodzic', $currentGroup->rodzic.'.%');
                        $where->notEqualTo('id' , $currentGroup->id);
                        $result = $this->tableGateway->select($where);
                        
                        foreach ($result as $current){
                            $data['rodzic'] = $grupy->rodzic.'.'.$currentGroup->id.str_replace($currentGroup->rodzic,"",$current->rodzic);
                            $this->update($data, $current->id);
                        }      
                    }else{
                        $lastParent = substr($currentGroup->rodzic, 0,-strlen($currentGroup->id)-1); 
                        $data['rodzic'] = $currentGroup->id;
                        $this->update($data, $currentGroup->id);

                        $where = new Where();    
                        $where->like('rodzic', $currentGroup->rodzic.'.%');
                        $where->notEqualTo('id' , $currentGroup->id);
                        $result = $this->tableGateway->select($where);
                        
                        foreach ($result as $current){
                            $data['rodzic'] = $currentGroup->id.str_replace($currentGroup->rodzic,"",$current->rodzic);
                            $this->update($data, $current->id);
                        }      
                    }    
                 }         
             } else {
                 throw new \Exception('Grupa nie istnieje');
             }
         }
         if($newParent != null)   $this->checkParent($newParent); 
         if($lastParent != null)  $this->checkParent($lastParent);

     }
     
     public function checkParent($id){

         $where = new Where();    
         $where->like('rodzic', $id.'.%');
         $result = $this->tableGateway->select($where);

         if(count($result)>0){
           $data = array('dziecko'  => 0);  
         }else{
           $data = array('dziecko'  => 1);   
         }
         $where = new Where();    
         $where->EqualTo('rodzic' , $id);
         $result = $this->tableGateway->select($where);
         $group = $result->current();
         $this->update($data, $group->id);
         
     }
     
     public function update($data, $id){
         $this->tableGateway->update($data, array('id' => $id));
     }
     public function delete($id)
     {
         $group = $this->getById($id);
         $where = new Where();    
         $where->like('rodzic', $group->rodzic.'.%');
         $result = $this->tableGateway->select($where);
         foreach ($result as $current){
             $this->tableGateway->delete(array('id' => (int) $current->id));
         }
         $this->tableGateway->delete(array('id' => (int) $id));
     }
 }