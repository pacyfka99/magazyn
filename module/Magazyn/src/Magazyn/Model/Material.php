<?php

 namespace Magazyn\Model;
 use Zend\InputFilter\InputFilter;
 use Zend\InputFilter\InputFilterAwareInterface;
 use Zend\InputFilter\InputFilterInterface;

 class Material
 {
     public $id;
     public $kod;
     public $nazwa;
     public $jednostka;
     public $grupa;
     public $id_jednostka_miary;
     public $id_grupy_materialow;
     protected $inputFilter;     

     public function exchangeArray($data)
     {
         $this->id     = (!empty($data['id'])) ? $data['id'] : null;
         $this->kod    = (!empty($data['kod'])) ? $data['kod'] : null;
         $this->nazwa  = (!empty($data['nazwa'])) ? $data['nazwa'] : null;
         $this->jednostka  = (!empty($data['jednostka'])) ? $data['jednostka'] : null;
         $this->grupa  = (!empty($data['grupa'])) ? $data['grupa'] : null;
         $this->id_jednostka_miary  = (!empty($data['id_jednostka_miary'])) ? $data['id_jednostka_miary'] : null;
         $this->id_grupy_materialow  = (!empty($data['id_grupy_materialow'])) ? $data['id_grupy_materialow'] : null;
     }
     
     public function setInputFilter(InputFilterInterface $inputFilter)
     {
         throw new \Exception("Not used");
     }
     
     public function getArrayCopy()
     {
         return get_object_vars($this);
     }

     public function getInputFilter()
     {
         if (!$this->inputFilter) {
             $inputFilter = new InputFilter();

             $inputFilter->add(array(
                 'name'     => 'id',
                 'required' => true,
                 'filters'  => array(
                     array('name' => 'Int'),
                 ),
             ));
                
             $inputFilter->add(array(
                 'name'     => 'kod',
                 'required' => true,
                 'filters'  => array(
                     array('name' => 'StripTags'),
                     array('name' => 'StringTrim'),
                 ),
                 'validators' => array(
                     array(
                         'name'    => 'StringLength',
                         'options' => array(
                             'encoding' => 'UTF-8',
                             'min'      => 1,
                             'max'      => 100,
                         ),
                     ),
                 ),
             ));
             
             $inputFilter->add(array(
                 'name'     => 'nazwa',
                 'required' => true,
                 'filters'  => array(
                     array('name' => 'StripTags'),
                     array('name' => 'StringTrim'),
                 ),
                 'validators' => array(
                     array(
                         'name'    => 'StringLength',
                         'options' => array(
                             'encoding' => 'UTF-8',
                             'min'      => 1,
                             'max'      => 100,
                         ),
                     ),
                 ),
             ));
             
            $inputFilter->add(array(
                 'name'     => 'id_jednostka_miary',
                 'required' => true,
                 'filters'  => array(
                     array('name' => 'Int'),
                 ),
             ));
                      
            $inputFilter->add(array(
                 'name'     => 'id_grupy_materialow',
                 'required' => true,
                 'filters'  => array(
                     array('name' => 'Int'),
                 ),
             ));

             $this->inputFilter = $inputFilter;
         }

         return $this->inputFilter;
     }

 }