<?php

 namespace Magazyn\Model;
 use Zend\Mvc\Controller\AbstractActionController;
 use Zend\Db\TableGateway\TableGateway;

 class JednostkiTable extends AbstractActionController
 {
     protected $tableGateway;

     public function __construct(TableGateway $tableGateway)
     {
  
         $this->tableGateway = $tableGateway;
     }

     public function fetchAll()
     {
         $resultSet = $this->tableGateway->select();
         return $resultSet;
     }

     public function getById($id)
     {
         $id  = (int) $id;
         $rowset = $this->tableGateway->select(array('id' => $id));
         $row = $rowset->current();
         if (!$row) {
             throw new \Exception("Could not find row $id");
         }
         return $row;
     }
//
     public function save(Jednostki $jednostki)
     {
         $data = array(
             'nazwa' => $jednostki->nazwa,
             'skrot'  => $jednostki->skrot,
         );

         $id = (int) $jednostki->id;
         if ($id == 0) {
             $this->tableGateway->insert($data);
         } else {
             if ($this->getById($id)) {
                 $this->tableGateway->update($data, array('id' => $id));
             } else {
                 throw new \Exception('Jednostka nie istnieje');
             }
         }
     }
     
     public function delete($id)
     {
         try{
         $this->tableGateway->delete(array('id' => (int) $id));
         }  catch (\Exception $e){
             $this->flashMessenger()->addErrorMessage('Nie można usunąć jednostki miary która jest przypisana do materiału');
         }
     }
 }