<?php

 namespace Magazyn\Model;
 
 use Zend\Db\TableGateway\TableGateway;
 use Zend\Db\Sql\Select;
 use Zend\Db\Sql\Expression;
 
 
 class MaterialTable
 {
     protected $tableGateway;

     public function __construct(TableGateway $tableGateway)
     {
         $this->tableGateway = $tableGateway;
     }

     public function fetchAll()
     {
         $resultSet = $this->tableGateway->select(function(Select $select){
               $select->join(array('j' => 'jednostka_miary'), 'material.id_jednostka_miary = j.id', array('jednostka' => 'nazwa'),'left');
               $select->join('grupa_materialow', 'material.id_grupy_materialow = grupa_materialow.id', array('grupa' => 'nazwa'),'left');   
       });

         return $resultSet;
     }
    
     public function getByid($id)
     {
         $id  = (int) $id;
         $rowset = $this->tableGateway->select(array('id' => $id));
         $row = $rowset->current();
         if (!$row) {
             throw new \Exception("Could not find row $id");
         }
         return $row;
     }
//
     public function save(Material $material)
     {
         $data = array(
             'kod' => $material->kod,
             'nazwa'  => $material->nazwa,
             'id_jednostka_miary'  => $material->id_jednostka_miary,
             'id_grupy_materialow'  => $material->id_grupy_materialow
         );

         $id = (int) $material->id;
         if ($id == 0) {
             $this->tableGateway->insert($data);
         } else {
             if ($this->getByid($id)) {
                 $this->tableGateway->update($data, array('id' => $id));
             } else {
                 throw new \Exception('Materiał nie istnieje');
             }
         }
     }

     public function delete($id)
     {
         $this->tableGateway->delete(array('id' => (int) $id));
     }
 }