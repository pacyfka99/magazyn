<?php

 namespace Magazyn\Model;
 use Zend\InputFilter\InputFilter;
 use Zend\InputFilter\InputFilterAwareInterface;
 use Zend\InputFilter\InputFilterInterface;

 class Jednostki
 {
     public $id;
     public $nazwa;
     public $jednostka;
     public $skrot;  
     protected $inputFilter;     
     
     public function exchangeArray($data)
     {
         $this->id     = (!empty($data['id'])) ? $data['id'] : null;
         $this->nazwa  = (!empty($data['nazwa'])) ? $data['nazwa'] : null;
         $this->jednostka  = (!empty($data['jednostka'])) ? $data['jednostka'] : null;
         $this->skrot    = (!empty($data['skrot'])) ? $data['skrot'] : null;
     }
     
     public function setInputFilter(InputFilterInterface $inputFilter)
     {
         throw new \Exception("Not used");
     }
     
     public function getArrayCopy()
     {
         return get_object_vars($this);
     }


     public function getInputFilter()
     {
         if (!$this->inputFilter) {
             $inputFilter = new InputFilter();

             $inputFilter->add(array(
                 'name'     => 'id',
                 'required' => true,
                 'filters'  => array(
                     array('name' => 'Int'),
                 ),
             ));
                
             $inputFilter->add(array(
                 'name'     => 'nazwa',
                 'required' => true,
                 'filters'  => array(
                     array('name' => 'StripTags'),
                     array('name' => 'StringTrim'),
                 ),
                 'validators' => array(
                     array(
                         'name'    => 'StringLength',
                         'options' => array(
                             'encoding' => 'UTF-8',
                             'min'      => 1,
                             'max'      => 100,
                         ),
                     ),
                 ),
             ));            

             $this->inputFilter = $inputFilter;
         }

         return $this->inputFilter;
     }

 }