<?php

namespace Magazyn\Form;

 use Zend\Form\Form;
 use Zend\Db\Sql\Select;
 class GrupyForm extends Form
 {   
     
     public function __construct($serviceLocator = null, $group = null)
     {
         // we want to ignore the name passed
         parent::__construct('grupy');
         $this->setAttribute('class','form-horizontal');
         $this->add(array(
             'name' => 'id',
             'type' => 'Hidden',
         ));


        $groups = $serviceLocator->fetchAllGroup(); 
        $groupsData = array( "brak" => "brak");
        foreach ($groups->toArray() as $current){
            if($group == null or (strpos($current['rodzic'], $group->rodzic.'.') === false and $current['rodzic'] != $group->rodzic)){
                $prefix = "";
                for ($i = 1; $i <= substr_count($current['rodzic'], '.'); $i++)
                {$prefix .= ' . ';}
                $groupsData[$current['rodzic']] = $prefix.$current['nazwa'];
            }
        }

         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'rodzic',
            'options' => array(
                'label' => 'Rodzic',
                'value_options' => $groupsData,
            ),
            'attributes' => array(
                'value' => 'null' ,
                'class' => 'form-control'
            )
      
        ));
         
        $this->add(array(
             'name' => 'nazwa',
             'type' => 'Text',
             'options' => array(
                 'label' => 'Grupa',
             ),
             'attributes' => array(
                'class' => 'form-control',
             ),
      ));
            
         $this->add(array(
             'name' => 'submit',
             'type' => 'Submit',
             'attributes' => array(
                 'value' => 'Wyślij',
                 'id' => 'submitbutton',
                 'class' => 'btn btn-default',
             ),
         ));
     }
    
 }

