<?php

namespace Magazyn\Form;

 use Zend\Form\Form;

 class JednostkiForm extends Form
 {
     public function __construct($name = null)
     {
         // we want to ignore the name passed
         parent::__construct('jednostki');
         
         $this->setAttribute('class','form-inline');
         
         $this->add(array(
             'name' => 'id',
             'type' => 'Hidden',
         ));
         $this->add(array(
             'name' => 'nazwa',
             'type' => 'Text',
             'options' => array(
                 'label' => 'Nazwa',
             ),
             'attributes' => array(
                'class' => 'form-control',
             ),
         ));
         $this->add(array(
             'name' => 'skrot',
             'type' => 'Text',
             'options' => array(
                 'label' => 'Skrót',
             ),
             'attributes' => array(
                'class' => 'form-control',
             ),
         ));
         $this->add(array(
             'name' => 'submit',
             'type' => 'Submit',
             'attributes' => array(
                 'value' => 'Wyślij',
                 'id' => 'submitbutton',
                 'class' => 'btn btn-default',
             ),
         ));
     }
 }

