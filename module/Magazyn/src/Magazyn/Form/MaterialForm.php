<?php

namespace Magazyn\Form;

 use Zend\Form\Form;

 class MaterialForm extends Form
 {
     public function __construct($serviceLocatorGrupy, $serviceLocatorJednostki)
     {
         parent::__construct('material');
         $this->setAttribute('class','form-horizontal');
         
         $this->add(array(
             'name' => 'id',
             'type' => 'Hidden',
         ));
        
         $this->add(array(
             'name' => 'kod',
             'type' => 'Text',
             'options' => array(
                 'label' => 'Kod',
             ),
             'attributes' => array(
                'class' => 'form-control',
             ),
         ));
         $this->add(array(
             'name' => 'nazwa',
             'type' => 'Text',
             'options' => array(
                 'label' => 'Nazwa',
             ),
             'attributes' => array(
                'class' => 'form-control',
             ),
         ));
         
        $grupy = $serviceLocatorGrupy->getChildren(); 
        $groupsData = array( "null" => "-wybierz-");
        foreach ($grupy->toArray() as $current){       
                $groupsData[$current['id']] = $current['nazwa'];
        }

         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'id_grupy_materialow',
            'options' => array(
                'label' => 'Grupa',
                'value_options' => $groupsData,
            ),
            'attributes' => array(
                'value' => 'null' ,
                'class' => 'form-control'
            )
        ));
         
         
        $jednostki = $serviceLocatorJednostki->fetchAll(); 
        $jednostkiData = array( "null" => "-wybierz-");
        foreach ($jednostki->toArray() as $current){       
                $jednostkiData[$current['id']] = $current['nazwa'];
        }

         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'id_jednostka_miary',
            'options' => array(
                'label' => 'Jednostka',
                'value_options' => $jednostkiData,
            ),
            'attributes' => array(
                'value' => 'null',
                'class' => 'form-control'
            )
        )); 
         
         $this->add(array(
             'name' => 'submit',
             'type' => 'Submit',
             'attributes' => array(
                 'value' => 'Wyślij',
                 'id' => 'submitbutton',
                 'class' => 'btn btn-default',
             ),
         ));
     }
 }

