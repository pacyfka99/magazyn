<?php
namespace Magazyn\Controller;

 use Zend\Mvc\Controller\AbstractActionController;
 use Zend\View\Model\ViewModel;
 use Magazyn\Model\Material;          
 use Magazyn\Form\MaterialForm;  

 class MaterialController extends AbstractActionController
 {
     protected $materialTable;
     protected $grupyTable;
     protected $jednostkiTable;
     
     public function indexAction()
     {
         return new ViewModel(array(
             'materials' => $this->getTable()->fetchAll(),
         ));
     }

     public function addAction()
     {
         $form = new MaterialForm($this->getGrupyTable(), $this->getJednostkiTable());
         $form->get('submit')->setValue('Dodaj');

         $request = $this->getRequest();
         if ($request->isPost()) {
             $material = new Material();
             $form->setInputFilter($material->getInputFilter());
             $form->setData($request->getPost());
             if ($form->isValid()) {
                 $material->exchangeArray($form->getData());
                 $this->getTable()->save($material);
//                 $this->getGrupyTable()->changeStatusChildren($material->id_grupy_materialow);
                 return $this->redirect()->toRoute('material');
             }
         }
         return array('form' => $form);

     }

     public function editAction()
     {
         $id = (int) $this->params()->fromRoute('id', 0);

         if (!$id) {
             return $this->redirect()->toRoute('material', array(
                 'action' => 'add'
             ));
         }
         try {
             $material = $this->getTable()->getById($id);
         }
         catch (\Exception $ex) {
             return $this->redirect()->toRoute('material', array(
                 'action' => 'index'
             ));
         }
         $form  = new MaterialForm($this->getGrupyTable(), $this->getJednostkiTable());

         $form->bind($material);
         $form->get('submit')->setAttribute('value', 'Zapisz');

         $request = $this->getRequest();
         if ($request->isPost()) {
             $form->setInputFilter($material->getInputFilter());
             $form->setData($request->getPost());

             if ($form->isValid()) {
                 $this->getTable()->save($material);
//                 $this->getGrupyTable()->changeStatusChildren($material->id_grupy_materialow);

                 return $this->redirect()->toRoute('material');
             }
         }

         return array(
             'id' => $id,
             'form' => $form,
         );
     }
    

     public function deleteAction()
     {
         $id = (int) $this->params()->fromRoute('id', 0);
         if (!$id) {
             return $this->redirect()->toRoute('material');
         }

         $request = $this->getRequest();
         if ($request->isPost()) {
             $del = $request->getPost('del', 'Nie');

             if ($del == 'Tak') {
                 $id = (int) $request->getPost('id');
                 $this->getTable()->delete($id);
             }

             return $this->redirect()->toRoute('material');
         }

         return array(
             'id'    => $id,
             'data' => $this->getTable()->getById($id)
         );
     }
     
     public function getTable()
     {
         if (!$this->materialTable) {
             $sm = $this->getServiceLocator();
             $this->materialTable = $sm->get('Magazyn\Model\MaterialTable');
             
         }
         return $this->materialTable;
     }
     public function getGrupyTable()
     {
         if (!$this->grupyTable) {
             $sm = $this->getServiceLocator();
             $this->grupyTable = $sm->get('Magazyn\Model\GrupyTable');
             
         }
         return $this->grupyTable;
     }
     public function getJednostkiTable()
     {
         if (!$this->jednostkiTable) {
             $sm = $this->getServiceLocator();
             $this->jednostkiTable = $sm->get('Magazyn\Model\JednostkiTable');
             
         }
         return $this->jednostkiTable;
     }
 }
