<?php
namespace Magazyn\Controller;

 use Zend\Mvc\Controller\AbstractActionController;
 use Zend\View\Model\ViewModel;
 use Magazyn\Model\Jednostki;      
 use Magazyn\Model\JednostkiTable;  
 use Magazyn\Form\JednostkiForm;  
 

 class JednostkiController extends AbstractActionController
 {
     protected $jednostkiTable;
     
     public function indexAction()
     {
         
         return new ViewModel(array(
             'data' => $this->getTable()->fetchAll(),
             'flashMessages' => $this->flashMessenger()->getErrorMessages(),
         ));
     }

     public function addAction()
     {
         $form = new JednostkiForm();
         $form->get('submit')->setValue('Dodaj');

         $request = $this->getRequest();
         if ($request->isPost()) {
             $jednostki = new Jednostki();
             $form->setInputFilter($jednostki->getInputFilter());
             $form->setData($request->getPost());

             if ($form->isValid()) {
                 $jednostki->exchangeArray($form->getData());
                 $this->getTable()->save($jednostki);

                 return $this->redirect()->toRoute('jednostki');
             }
         }
         return array('form' => $form);

     }

     public function editAction()
     {
         $id = (int) $this->params()->fromRoute('id', 0);

         if (!$id) {
             return $this->redirect()->toRoute('jednostki', array(
                 'action' => 'add'
             ));
         }
         try {
             $jednostki = $this->getTable()->getById($id);
         }
         catch (\Exception $ex) {
             return $this->redirect()->toRoute('jednostki', array(
                 'action' => 'index'
             ));
         }

         $form  = new JednostkiForm();
         $form->bind($jednostki);
         $form->get('submit')->setAttribute('value', 'Zapisz');

         $request = $this->getRequest();
         if ($request->isPost()) {
             $form->setInputFilter($jednostki->getInputFilter());
             $form->setData($request->getPost());

             if ($form->isValid()) {
                 $this->getTable()->save($jednostki);

                 return $this->redirect()->toRoute('jednostki');
             }
         }

         return array(
             'id' => $id,
             'form' => $form,
         );
     }

     public function deleteAction()
     {
         $id = (int) $this->params()->fromRoute('id', 0);
         if (!$id) {
             return $this->redirect()->toRoute('jednostki');
         }

         $request = $this->getRequest();
         if ($request->isPost()) {
             $del = $request->getPost('del', 'Nie');

             if ($del == 'Tak') {
                 $id = (int) $request->getPost('id');
                 $this->getTable()->delete($id);
             }

             return $this->redirect()->toRoute('jednostki');
         }

         return array(
             'id'    => $id,
             'data' => $this->getTable()->getById($id)
         );
     }
     
     public function getTable()
     {
         if (!$this->jednostkiTable) {
             $sm = $this->getServiceLocator();
             $this->jednostkiTable = $sm->get('Magazyn\Model\JednostkiTable');
         }
         return $this->jednostkiTable;
     }
 }
