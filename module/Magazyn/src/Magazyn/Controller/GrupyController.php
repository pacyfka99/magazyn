<?php
namespace Magazyn\Controller;

 use Zend\Mvc\Controller\AbstractActionController;
 use Zend\View\Model\ViewModel;
 use Magazyn\Model\Grupy;      
 use Magazyn\Model\GrupyTable;  
 use Magazyn\Form\GrupyForm;  
 

 class GrupyController extends AbstractActionController
 {
     protected $grupyTable;
     
     public function indexAction()
     {
         return new ViewModel(array(
             'data' => $this->getTable()->fetchAll(),
         ));
     }

     public function addAction()
     {
         $form = new GrupyForm($this->getTable());
         $form->get('submit')->setValue('Dodaj');

         $request = $this->getRequest();
         if ($request->isPost()) {
             $grupy = new Grupy();
             $form->setInputFilter($grupy->getInputFilter());
             $form->setData($request->getPost());

             if ($form->isValid()) {
                 $grupy->exchangeArray($form->getData());
                 $this->getTable()->save($grupy);

                 return $this->redirect()->toRoute('grupy');
             }
         }
         return array('form' => $form);

     }

     public function editAction()
     {
         $id = (int) $this->params()->fromRoute('id', 0);

         if (!$id) {
             return $this->redirect()->toRoute('grupy', array(
                 'action' => 'add'
             ));
         }
         try {
             $grupy = $this->getTable()->getById($id);
         }
         catch (\Exception $ex) {
             return $this->redirect()->toRoute('grupy', array(
                 'action' => 'index'
             ));
         }
         $form  = new GrupyForm($this->getTable(), $grupy);
         if(substr_count($grupy->rodzic, '.') == 0) {
             $grupy->rodzic = 'brak';
         }else{
             $grupy->rodzic = substr($grupy->rodzic, 0,-strlen($grupy->id)-1);    
         }
      
         $form->bind($grupy);
         $form->get('submit')->setAttribute('value', 'Zapisz');

         $request = $this->getRequest();
         if ($request->isPost()) {
             $form->setInputFilter($grupy->getInputFilter());
             $form->setData($request->getPost());

             if ($form->isValid()) {
                 $this->getTable()->save($grupy);

                 return $this->redirect()->toRoute('grupy');
             }
         }

         return array(
             'id' => $id,
             'form' => $form,
         );
     }

     public function deleteAction()
     {
         $id = (int) $this->params()->fromRoute('id', 0);
         if (!$id) {
             return $this->redirect()->toRoute('grupy');
         }

         $request = $this->getRequest();
         if ($request->isPost()) {
             $del = $request->getPost('del', 'Nie');

             if ($del == 'Tak') {
                 $id = (int) $request->getPost('id');
                 $this->getTable()->delete($id);
             }

             return $this->redirect()->toRoute('grupy');
         }

         return array(
             'id'    => $id,
             'data' => $this->getTable()->getById($id)
         );
     }

     public function getTable()
     {
         if (!$this->grupyTable) {
             $sm = $this->getServiceLocator();
             $this->grupyTable = $sm->get('Magazyn\Model\GrupyTable');
         }
         return $this->grupyTable;
     }
 }
